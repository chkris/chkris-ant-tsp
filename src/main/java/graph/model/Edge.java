package graph.model;

public abstract class Edge {
    
    public abstract int getId();
    public abstract void setWeight(int weight);
    public abstract int getWeight();
    public abstract Vertex getSource();
    public abstract void setSource(Vertex source);
    public abstract Vertex getDestination();
    public abstract void setDestination(Vertex target);
    public abstract boolean contains(Vertex vertex);
    public abstract double getPheromone();
    public abstract void setPheromone(double pheromone);
}
