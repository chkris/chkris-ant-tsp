
package graph.model.directed;
 
import graph.model.Vertex;

 
public class DirectedVertex implements Vertex{

    private int id;

    public DirectedVertex(int id)  {
        this.id = id;
    }
    
    @Override
    public int getId() {
        return this.id;
    }    
}
