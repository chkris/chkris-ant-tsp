package graph.model.directed;

import graph.model.Edge;
import graph.model.Vertex;

public class DirectedEdge extends Edge {

    private int id;
    private Vertex source;
    private Vertex destination;
    private int weight;
    private double pheromone;

    public DirectedEdge(int id, Vertex source, Vertex destination, int weight, double pheromone) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
        this.pheromone = pheromone;
    }

    public double getPheromone() {
        return pheromone;
    }

    public void setPheromone(double pheromone) {
        this.pheromone = pheromone;
    }
    
    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }  
    
    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Vertex getSource() {
        return this.source;
    }

    @Override
    public void setSource(Vertex source) {
        this.source = source;
    }

    @Override
    public Vertex getDestination() {
        return this.destination;
    }

    @Override
    public void setDestination(Vertex destination) {
        this.destination = destination;
    }
    
    @Override
    public boolean contains(Vertex vertex) {
        if(source == vertex){
            return true;
        }
        return false;
    }


    
}
