package graph.representation.directed;

import graph.Graph;
import graph.model.Edge;
import graph.model.Vertex;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class DirectedWeightedGraph implements Graph {

    private List<Vertex> vertices;
    private List<Edge> edges;

    public DirectedWeightedGraph() {
        this.vertices = new ArrayList();
        this.edges = new ArrayList();
    }

    @Override
    public void addVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }
    
    @Override
    public void addEdges(List<Edge> edges) {
        this.edges = edges;
    }
    
    public List<Vertex> getVertices() {
        return this.vertices;
    }
    
    public List<Edge> getEdges() {
        return this.edges;
    }
    
    @Override
    public void addEdge(Edge edge) {
        this.edges.add(edge);
    }

    @Override
    public void addVertex(Vertex vertex) {
        this.vertices.add(vertex);
    }
    
    public void removeEdge(Edge edge) {
        this.edges.remove(edge);
    }
    
    public synchronized void removeVertex(Vertex vertex){
        List<Edge> toRemove = new ArrayList<>();
        for(Edge edge : edges){
            if(edge.contains(vertex)) {
                toRemove.add(edge);
            }
        }
        edges.removeAll(toRemove);
        vertices.remove(vertex);
    }
   
    @Override
    public List<Edge> getEdgesByVertex(Vertex vertex) {
        List<Edge> edgesConectedToVertex = new ArrayList();
        
        for(Edge edge : edges) {
            if(edge.contains(vertex)) {
                edgesConectedToVertex.add(edge);
            }
        }
                
        return edgesConectedToVertex;
    }

    @Override
    public boolean hasNoEdgesVertex(Vertex vertex) {
        return !getEdgesByVertex(vertex).isEmpty();
    }

    @Override
    public List<Edge> getEdgesPointingToVertex(Vertex vertex) {
        List<Edge> tmpEdges = new ArrayList();
        
        for(Edge edge: this.getEdges()) {
            if(edge.getDestination().getId() == vertex.getId()) {
                tmpEdges.add(edge);
            }
        }
        
        return tmpEdges;
    }

    @Override
    public Edge getEdgeBetweenToVertices(Vertex iVertex, Vertex jVertex) {

        for(Edge edge : getEdges()) {
            if (edge.getSource() == iVertex && edge.getDestination() == jVertex) {

                return edge;
            }
        }

        return null;
    }

    public Vertex getRandomVertex()
    {
        Random randomGenerator = new Random();

        List<Edge> edgeList = getEdges();

        return edgeList.get(randomGenerator.nextInt(edgeList.size())).getSource();
    }
}
