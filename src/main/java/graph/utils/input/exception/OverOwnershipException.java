package graph.utils.input.exception;

import graph.model.Vertex;

public class OverOwnershipException extends Exception {

    public OverOwnershipException(Vertex vertex, int sumOfWeights) {
        super(String.format(
                "To vertex id=%s cannot points edges with sum greater than 100. Current sum of weights=%s", vertex.getId() + 1, sumOfWeights));
    }
}
