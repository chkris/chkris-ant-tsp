package graph.utils.input.exception;

import graph.model.Edge;

public class CompanyCannotContainItselfException extends Exception{

    public CompanyCannotContainItselfException(Edge edge) {
        super(String.format(
                "Vertex id=%s cannot ownership themself with weight=%s", edge.getSource().getId(), edge.getWeight()));
    }
}