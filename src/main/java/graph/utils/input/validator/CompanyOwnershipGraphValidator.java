package graph.utils.input.validator;

import graph.Graph;
import graph.model.Edge;
import graph.model.Vertex;
import graph.utils.input.exception.CompanyCannotContainItselfException;
import graph.utils.input.exception.OverOwnershipException;

public class CompanyOwnershipGraphValidator implements GraphValidator {

    public static final int OWENERSHIP_LEVEL = 50;
    public static final int MAX_OWNERSHIP_SUM_WEIGHT = 100;
    /**
     * Validator for imput data
     * 
     * @param graph
     * @throws OverOwnershipException
     * @throws CompanyCannotContainItselfException require imput data to be
     * valid, even if not directlly can be recognized that to company(vertex)
     * points edges with sum of weights more than 50
     */
    @Override
    public void validate(Graph graph) throws OverOwnershipException, CompanyCannotContainItselfException {

        for (Vertex vertex : graph.getVertices()) {
            int sum = 0;
            for (Edge edge : graph.getEdgesPointingToVertex(vertex)) {
                sum += edge.getWeight();
            }
            if (sum > MAX_OWNERSHIP_SUM_WEIGHT) {
                throw new OverOwnershipException(vertex, sum);
            }
        }

        for (Edge edge : graph.getEdges()) {
            if (edge.getWeight() > OWENERSHIP_LEVEL && edge.getDestination().equals(edge.getSource())) {
                throw new CompanyCannotContainItselfException(edge);
            }
        }
    }
}
