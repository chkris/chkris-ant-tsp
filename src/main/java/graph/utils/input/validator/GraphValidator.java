package graph.utils.input.validator;

import graph.Graph;
import graph.utils.input.exception.CompanyCannotContainItselfException;
import graph.utils.input.exception.OverOwnershipException;

public interface GraphValidator {
    
    public void validate(Graph graph) throws OverOwnershipException, CompanyCannotContainItselfException;
}
