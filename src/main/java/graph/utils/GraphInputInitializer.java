package graph.utils;

import graph.Graph;
import graph.model.Edge;
import graph.model.Vertex;
import graph.model.directed.DirectedEdge;
import graph.model.directed.DirectedVertex;
import graph.representation.directed.DirectedWeightedGraph;

import java.util.ArrayList;
import java.util.List;

public class GraphInputInitializer {

    public List<Vertex> vertices;
    public List<Edge> edges;
    public Graph graph;

    private int searchMaximumVertexIndex(List<List<Integer>> input) {
        //vertices are indexed from 1
        int maximalIndexValue = 0;

        for (List<Integer> line : input) {
            if (line.get(0) > maximalIndexValue) {
                maximalIndexValue = line.get(0);
            }
            if (line.get(1) > maximalIndexValue) {
                maximalIndexValue = line.get(1);
            }
        }

        return maximalIndexValue;
    }
    
    public Graph initialize(List<List<Integer>> input) {
        this.graph = new DirectedWeightedGraph();
        vertices = new ArrayList();
        edges = new ArrayList();
        
        int maximalIndexValue = searchMaximumVertexIndex(input);

        for (int i = 0; i < maximalIndexValue; i++) {
            
            Vertex v = new DirectedVertex(i);
            vertices.add(v);
        }

        int i = 0;
        for (List<Integer> line : input) {
            Edge edge = new DirectedEdge(i++ ,vertices.get(line.get(0) - 1), vertices.get(line.get(1) - 1), line.get(2), line.get(3));
            edges.add(edge);
        }

        graph.addVertices(vertices);
        graph.addEdges(edges);

        return graph;
    }
}
