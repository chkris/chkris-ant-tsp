package graph;

import graph.model.Edge;
import graph.model.Vertex;
import java.util.List;

public interface Graph {
    
    public void addEdge(Edge edge);
    public void addVertex(Vertex vertex);
    public void addEdges(List<Edge> edges);
    public void addVertices(List<Vertex> vertices);
    public List<Vertex> getVertices();
    public List<Edge> getEdges();
    public List<Edge> getEdgesByVertex(Vertex vertex);
    public List<Edge> getEdgesPointingToVertex(Vertex vertex);
    public Edge getEdgeBetweenToVertices(Vertex iVertex, Vertex jVertex);
    public boolean hasNoEdgesVertex(Vertex vertex);
    public Vertex getRandomVertex();
}
