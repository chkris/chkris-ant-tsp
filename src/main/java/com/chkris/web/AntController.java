package com.chkris.web;

import com.chkris.service.AntService;
import org.springframework.beans.factory.annotation.Autowired;
import com.chkris.ant.builder.AntAlgorithmSettingsBuilder;
import com.chkris.ant.builder.GraphBuilder;
import com.chkris.ant.model.AntAlgorithmSettings;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AntController {

    private final AntService antService;

    private final AntAlgorithmSettingsBuilder antAlgorithmSettingsBuilder;

    private final GraphBuilder graphBuilder;

	@Autowired
	public AntController(AntService antService, AntAlgorithmSettingsBuilder antAlgorithmSettingsBuilder, GraphBuilder graphBuilder) {
        this.antService = antService;
        this.antAlgorithmSettingsBuilder = antAlgorithmSettingsBuilder;
        this.graphBuilder = graphBuilder;
	}

    @RequestMapping(method = RequestMethod.POST, value="/city", consumes = "application/json")
    @ResponseBody
    public String processSubmitCreateClient(@RequestBody String request) throws Exception {

        AntAlgorithmSettings antAlgorithmSettings = this.antAlgorithmSettingsBuilder.buildFromRequest(request);

        this.antService.setGraph(this.graphBuilder.build(request, antAlgorithmSettings));
        this.antService.setCount(0);
        this.antService.setAntAlgorithmSettings(antAlgorithmSettings);
        return antAlgorithmSettings.toString();
    }
}
