package com.chkris.service;

import graph.Graph;
import graph.model.Edge;
import graph.model.Vertex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import com.chkris.service.AntDecisionAlgorithm;
import com.chkris.ant.model.AntAlgorithmSettings;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AntService {

    private final MessageSendingOperations<String> messagingTemplate;

    private Graph graph = null;
    private Graph tmpGraph = null;
    private AntAlgorithmSettings antAlgorithmSettings;
    private int count;

    @Autowired
    public AntService(MessageSendingOperations<String> messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void setAntAlgorithmSettings(AntAlgorithmSettings antAlgorithmSettings) {
        this.antAlgorithmSettings = antAlgorithmSettings;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
        this.tmpGraph = graph;
    }

    @Scheduled(fixedDelay=100)
    public void sentAnt() {

        if (null != this.graph && this.count < antAlgorithmSettings.getMaximalNumberOfIterations()) {
            this.count++;

            Graph g = this.graph;
            AntDecisionAlgorithm antDecisionAlgorithm = new AntDecisionAlgorithm(antAlgorithmSettings);
            antDecisionAlgorithm.sentAllAnts(g);

            for (Edge edge : g.getEdges()) {
                this.tmpGraph.getEdges().get(edge.getId()).setPheromone(tmpGraph.getEdges().get(edge.getId()).getPheromone() + edge.getPheromone());
            }

            if (this.count == antAlgorithmSettings.getMaximalNumberOfIterations() - 1) {
                this.graph = null;
                this.count = 0;
            }

            this.messagingTemplate.convertAndSend("/topic/ant", this.prepareDataForPresentationLayer(g));
        }
    }

    protected List<Double> prepareDataForPresentationLayer(Graph graph) {
        List<Double> edges = new ArrayList<Double>();
        Integer numberOfVertices = graph.getVertices().size();

        for(Vertex vertex : graph.getVertices()) {
            Integer tmpNumberOfVertices = numberOfVertices + vertex.getId();

            for(Edge edge : graph.getEdgesByVertex(vertex)) {
                if (tmpNumberOfVertices - numberOfVertices > 0) {
                    edges.add(edge.getPheromone());

                    tmpNumberOfVertices--;
                }
            }
        }

        return edges;
    }
}
