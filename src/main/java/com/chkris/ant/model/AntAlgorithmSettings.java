package com.chkris.ant.model;

public class AntAlgorithmSettings {

    private Double alpha;
    private Double beta;
    private Integer initialPheromoneAmount;
    private Integer maximalNumberOfAnts;
    private Integer maximalNumberOfIterations;
    private Double evaporationCoefficient;

    public AntAlgorithmSettings() {
    }

    public Double getAlpha() {
        return alpha;
    }

    public AntAlgorithmSettings setAlpha(Double alpha) {
        this.alpha = alpha;

        return this;
    }

    public Double getBeta() {
        return beta;
    }

    public AntAlgorithmSettings setBeta(Double beta) {
        this.beta = beta;

        return this;
    }

    public Integer getInitialPheromoneAmount() {
        return initialPheromoneAmount;
    }

    public AntAlgorithmSettings setInitialPheromoneAmount(Integer initialPheromoneAmount) {
        this.initialPheromoneAmount = initialPheromoneAmount;

        return this;
    }

    public Integer getMaximalNumberOfAnts() {
        return maximalNumberOfAnts;
    }

    public AntAlgorithmSettings setMaximalNumberOfAnts(Integer maximalNumberOfAnts) {
        this.maximalNumberOfAnts = maximalNumberOfAnts;

        return this;
    }

    public Integer getMaximalNumberOfIterations() {
        return maximalNumberOfIterations;
    }

    public AntAlgorithmSettings setMaximalNumberOfIterations(Integer maximalNumberOfIterations) {
        this.maximalNumberOfIterations = maximalNumberOfIterations;

        return this;
    }

    public Double getEvaporationCoefficient() {
        return evaporationCoefficient;
    }

    public AntAlgorithmSettings setEvaporationCoefficient(Double evaporationCoefficient) {
        this.evaporationCoefficient = evaporationCoefficient;

        return this;
    }

    public String toString() {
        return "alpha: " + alpha +
               " beta: " + beta +
               " initialPheromoneAmount: " + initialPheromoneAmount +
               " maximalNumberOfAnts: " + maximalNumberOfAnts +
               " maximalNumberOfIterations: " + maximalNumberOfIterations +
               " evaporationCoefficient: " + evaporationCoefficient;
    }
}
