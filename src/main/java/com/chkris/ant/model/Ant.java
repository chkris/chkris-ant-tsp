package com.chkris.ant.model;

import graph.model.Edge;
import graph.model.Vertex;

import java.util.ArrayList;
import java.util.List;

public class Ant {

    private List<Vertex> listOfVisitedVertices= new ArrayList<>();
    private List<Edge> edges = new ArrayList<>();

    private double tour;


    public List<Edge> getEdges() {
        return edges;
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    public List<Vertex> getListOfVisitedVertices() {
        return listOfVisitedVertices;
    }

    public void setListOfVisitedVertices(List<Vertex> listOfVisitedVertices) {
        this.listOfVisitedVertices = listOfVisitedVertices;
    }

    public void addVisitedVertex(Vertex vertex) {
        this.listOfVisitedVertices.add(vertex);
    }

    public void addToTour(double tour) {
        this.tour += tour;
    }

    public double getTour() {
        return tour;
    }

    public void zeroTour() {
        tour = 0;
        edges = new ArrayList<>();
    }
}
