package com.chkris.ant.builder;

import com.chkris.ant.model.AntAlgorithmSettings;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.JsonNode;

import org.springframework.stereotype.Service;

@Service
public class AntAlgorithmSettingsBuilder {

    public AntAlgorithmSettings buildFromRequest(String request) {
        AntAlgorithmSettings antAlgorithmSettings = new AntAlgorithmSettings();

        ObjectMapper mapper = new ObjectMapper();

        try {
            JsonNode antAlgorithmSettingsJsonNode = mapper.readTree(request).get("antAlgorithmSettings");

            antAlgorithmSettings
                .setAlpha(antAlgorithmSettingsJsonNode.get("alpha").getValueAsDouble())
                .setBeta(antAlgorithmSettingsJsonNode.get("beta").getValueAsDouble())
                .setInitialPheromoneAmount(antAlgorithmSettingsJsonNode.get("initialPheromoneAmount").getValueAsInt())
                .setMaximalNumberOfAnts(antAlgorithmSettingsJsonNode.get("maximalNumberOfAnts").getValueAsInt())
                .setMaximalNumberOfIterations(antAlgorithmSettingsJsonNode.get("maximalNumberOfIterations").getValueAsInt())
                .setEvaporationCoefficient(antAlgorithmSettingsJsonNode.get("evaporationCoefficient").getValueAsDouble());
        } catch (Exception e) {
            //log e
        }

        return antAlgorithmSettings;
    }
}
