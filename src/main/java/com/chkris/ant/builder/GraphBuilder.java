package com.chkris.ant.builder;

import graph.Graph;
import graph.utils.GraphInputInitializer;
import com.chkris.ant.model.AntAlgorithmSettings;
import com.chkris.ant.model.City;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

@Service
public class GraphBuilder {

    public Graph build(String request, AntAlgorithmSettings antAlgorithmSettings) {
        ObjectMapper mapper = new ObjectMapper();

        ArrayList cities = null;
        List<List<Integer>> input = new ArrayList();

        try {
            cities = mapper.readValue(mapper.readTree(request).get("cities"), TypeFactory.collectionType(ArrayList.class, City.class));

            for (Object cityObject : cities) {
                for (Object cityObject2 : cities) {
                    if (cityObject != cityObject2) {
                        input.add(this.singleCity(cityObject, cityObject2, cities, antAlgorithmSettings));
                    }
                }
            }
            GraphInputInitializer gii = new GraphInputInitializer();
            return gii.initialize(input);
        }
        catch (Exception e) {
            //log e
        }

        return null;
    }

    protected Double length(Integer p1x, Integer p1y, Integer p2x, Integer p2y) {
        return Math.sqrt(Math.pow(p1x - p2x, 2.0) + Math.pow(p1y - p2y, 2.0));
    }

    protected List singleCity(Object cityObjOne, Object cityObjTwo, List cities, AntAlgorithmSettings antAlgorithmSettings) {

        City city1 = (City)cityObjOne;
        City city2 = (City)cityObjTwo;

        List<Integer> line = new ArrayList();
        line.add(cities.indexOf(cityObjOne) + 1);
        line.add(cities.indexOf(cityObjTwo) + 1);
        line.add(this.length(city1.getPositionX(), city1.getPositionY(), city2.getPositionX(), city2.getPositionY()).intValue());
        line.add(antAlgorithmSettings.getInitialPheromoneAmount());

        return line;
    }
}
