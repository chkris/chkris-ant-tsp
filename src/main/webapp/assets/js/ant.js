function ApplicationModel(stompClient) {
  var self = this;

  self.connect = function() {
    stompClient.connect('', '', function(frame) {

    stompClient.subscribe("/topic/ant", function(message) {
        var arr = JSON.parse(message.body);

        var count = 0;
        lines.forEach(function(line) {
            line.setOpacity(arr[count]/Math.max.apply(Math, arr));
            count++;
        });

        layer.draw();
    });

    }, function(error) {
      console.log("STOMP protocol error " + error);
    });
  }

  self.logout = function() {
    stompClient.disconnect();
    window.location.href = "../logout.html";
  }
}

/************ model part **************/

var layer = new Kinetic.Layer();

function City(positionX, positionY) {

    this.positionX = positionX;
    this.positionY = positionY;

    this.getPositionX = function() { return positionX }
    this.getPositionY = function() { return positionY }
}

var cities = new Array();
var lines = new Array();

function createCity(mousePosition) {
    var city = new City(mousePosition.x, mousePosition.y);
    str = "";
    cities.forEach(function(cityTarget) {
        var middle = calculateMiddle(city, cityTarget);

        var line = new Kinetic.Line({
            points: [city.getPositionX(), city.getPositionY(), cityTarget.getPositionX(), cityTarget.getPositionY()],
            stroke: 'blue',
            strokeWidth: 10,
            opacity: 0.2
        });

        var text = new Kinetic.Text({
            x: middle["newX"],
            y: middle["newY"],
            fontFamily: 'Calibri',
            fontSize: 10,
            text: lines.length,
            fill: 'red'
        });

        lines.push(line);
        layer.add(text);
        layer.add(line);
    });

    cities.push(city);
}

function calculateMiddle(city, cityTarget) {
    if (city.getPositionY() - cityTarget.getPositionY() > 0) {
        newY = cityTarget.getPositionY() + Math.round(Math.abs(city.getPositionY() - cityTarget.getPositionY()) / 2);
    } else {
        newY = city.getPositionY() + Math.round(Math.abs(city.getPositionY() - cityTarget.getPositionY()) / 2);
    }

    if (city.getPositionX() - cityTarget.getPositionX() > 0) {
        newX = cityTarget.getPositionX() + Math.round(Math.abs(city.getPositionX() - cityTarget.getPositionX()) / 2);
    } else {
        newX = city.getPositionX() + Math.round(Math.abs(city.getPositionX() - cityTarget.getPositionX()) / 2);
    }

    return {
        "newX" : newX,
        "newY" : newY
    };
}

var text = new Kinetic.Text({
    x: 10,
    y: 10,
    fontFamily: 'Calibri',
    fontSize: 24,
    text: '',
    fill: 'white'
});

var stage = new Kinetic.Stage({
    container: 'container',
    width: 800,
    height: 600
});

var rect = new Kinetic.Rect({
    x: 0,
    y: 0,
    width: 800,
    height: 600,
    fill: 'white',
    stroke: 'black',
    strokeWidth: 0
});

rect.on('mousedown', function() {
    var mousePos = stage.getPointerPosition();

    var city = new Kinetic.Circle({
        x: mousePos.x,
        y: mousePos.y,
        radius: 15,
        fill: 'blue',
        stroke: 'blue',
        strokeWidth: 0,
        opacity: 1
    });

    createCity(mousePos);

    layer.add(city);
    layer.draw();
});

layer.add(rect);
layer.add(text);
stage.add(layer);

$( document ).ready(function() {
    $("#compute").click(function() {
        $.ajax({
            url: '/ant/city',
            type: 'POST',
            data: JSON.stringify({
            "cities": cities,
            "antAlgorithmSettings": {
                "alpha": $("#alpha").val(),
                "beta": $("#beta").val(),
                "initialPheromoneAmount":$("#initialPheromoneAmount").val(),
                "maximalNumberOfAnts": $("#maximalNumberOfAnts").val(),
                "maximalNumberOfIterations": $("#maximalNumberOfIterations").val(),
                "evaporationCoefficient": $("#evaporationCoefficient").val()
            }
            }),
            contentType: 'application/json',
            error: function (jqXHR, textStatus, errorThrown) {
            alert('An error has occured!! :-(')
            }
        });
    });
});

